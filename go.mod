module bitbucket.org/thoomycz/gofpdf

go 1.12

require (
	github.com/boombuler/barcode v1.0.1
	github.com/phpdave11/gofpdi v1.0.13
	github.com/pkg/errors v0.9.1 // indirect
	github.com/ruudk/golang-pdf417 v0.0.0-20181029194003-1af4ab5afa58
	golang.org/x/image v0.14.0
)

